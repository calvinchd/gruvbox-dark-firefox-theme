# Gruvbox Dark Firefox Theme
I made this theme since all the ones I found were either incomplete (certain panels were still default themed) or non-functional for the current version of Firefox and Thunderbird.
## Downloads
[Firefox](https://addons.mozilla.org/en-US/firefox/addon/gruvbox-dark-theme/)

[Thunderbird](https://addons.thunderbird.net/en-US/thunderbird/addon/gruvbox-dark-thunderbird/)
## Credits
[Gruvbox](https://github.com/morhetz/gruvbox) - [MIT License](https://en.wikipedia.org/wiki/MIT_License)
